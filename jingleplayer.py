import os
import sys
import kivy
import pygame
import datetime

kivy.require('1.0.8')
pygame.init()

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.core.audio import SoundLoader
from kivy.properties import StringProperty, ObjectProperty, NumericProperty
from glob import glob
from os.path import dirname, join, basename
from kivy.core.window import Window
from kivy.clock import Clock

root = ''
Window.fullscreen = False
currentfile = False
mainbutton = False
progressbar = False
interval = False

class AudioButton(Button):
    global mainbutton
    global currentfile
    
    filename = StringProperty(None)
    sound = ObjectProperty(None, allownone=True)
         
    def on_press(self):
        global currentfile
        currentfile = self.filename
        AudioApp.clearchildren('foo')
        self.background_color = (1.0, 0.0, 0.0, 1.0)
        mainbutton.text = os.path.basename(currentfile).replace('.wav','')
        mainbutton.background_color = (0.0, 1.0, 0.0, 1)

class AudioBackground(BoxLayout):
    pass


class AudioApp(App):

    def build(self):
        global mainbutton
        global progressbar
        global root
        
        root = AudioBackground(spacing=5)
        
        for fn in glob(join(dirname('dropbox/'), '*.wav')):
            btn = AudioButton(
                text=basename(fn[:-4]).replace('_', ' '), filename=fn,
                size_hint=(None, None), halign='center',
                size=(128, 128), text_size=(100, None))
            root.ids.sl.add_widget(btn)

        mainbutton = root.ids.primer
        mainbutton.background_color = (0.0, 1.0, 0.0, 0.2)
        progressbar = root.ids.pb
            
        return root

    def play_audio(self):
        global currentfile
        global alength
        global interval
        global root
        global mainbutton
        
        self.release_audio()
        mainbutton.background_color = (.6, .6, .6, 1.0)

        pygame.mixer.music.load(currentfile)
        asound = pygame.mixer.Sound(currentfile)
        alength = asound.get_length() * 1000
        pygame.mixer.music.play(0)
        interval = Clock.schedule_interval(self.checkposition, 1 / 30.)
        
    def checkposition(self,foo):
        global mainbutton
        global root
        
        pos = pygame.mixer.music.get_pos()
        perc = round((pos*100)/alength)                    
        self.root.ids.pb.value = perc
        timelabel = str(datetime.timedelta(milliseconds=pos))
        timelabel = timelabel[2:10]
        curpos = str(datetime.timedelta(milliseconds=alength))
        timelabel += " - " + curpos[2:10]
        root.ids.timelabel.text = str(timelabel);
        
        if( pos == -1):
            root.ids.timelabel.text = '00:00:00 - ' + curpos[2:10]
            mainbutton.background_color = (0.0, 1.0, 0.0, 1)
            self.release_audio()
    
    def clearchildren(self):
        global root
        for audiobutton in root.ids.sl.children:
            audiobutton.background_color = (.3, .3, .3, 1.0)    
    
    def release_audio(self):
        global interval
        global root
        
        if( interval != False ):
            interval.cancel()
            Clock.unschedule(interval)
        interval = False
        
        pygame.mixer.music.stop()

    def stop_audio(self):
        global root
        root.ids.primer.background_color = (0.0, 1.0, 0.0, 1)
        self.clearchildren()
        self.release_audio()        
   
    def toggleFullscreen(foo):
        if( Window.fullscreen ):
            Window.fullscreen = False
        else :
            Window.fullscreen = True

if __name__ == '__main__':
    AudioApp().run()

