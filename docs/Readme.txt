/*
   JinglePlayer v1.0 voor Raspberry Pi. 2018 Thomas Heijmans thomas.heijmans@newartsint.com

   Libraries: Kivy
*/

Plaats alle jingles in de map 'dropbox'

	Specificaties:
	
		bestand: wav 
		frequentie: 44.1 Khz 
		kanalen: stereo 

Foutje ?

	Als een bestand niet kan worden afgespeeld, wordt deze in de dropbox/rejected map geplaatst
	Het bestand kan beschadigd zijn of niet de juiste encodering hebben.

	De beste gratis converter is:
	https://www.mediahuman.com/audio-converter/


